import React, { Component } from "react";
import {
  StyleSheet,
  Platform,
  View,
  ActivityIndicator,
  FlatList,
  Text,
  Image,
  Alert
} from "react-native";
export default class Project extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true
    };
  }

  GetItem(drink_name) {
    Alert.alert(drink_name);
  }

  FlatListItemSeparator = () => {
    return (
      <View
        style={{
          height: 0.5,
          width: "100%",
          backgroundColor: "#000"
        }}
      />
    );
  };

  webCall = () => {
    return fetch(
      "https://www.thecocktaildb.com/api/json/v1/1/filter.php?g=Cocktail_glass"
    )
      .then(response => response.json())
      .then(responseJson => {
        this.setState({
          isLoading: false,
          dataSource: responseJson
        });
      })
      .catch(error => {
        console.error(error);
      });
  };

  componentDidMount() {
    this.webCall();
  }

  render() {
    if (this.state.isLoading) {
      return (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" />
        </View>
      );
    }
    return (
      <View style={styles.MainContainer}>
        <FlatList
          data={this.state.dataSource.drinks}
          ItemSeparatorComponent={this.FlatListItemSeparator}
          renderItem={({ item }) => (
            <View style={{ flex: 1, flexDirection: "row" }}>
              <Text
                onPress={this.GetItem.bind(this, item.strDrink)}
                style={styles.textView}
              >
                {item.strDrink}
              </Text>
              <Image
                source={{ uri: item.strDrinkThumb }}
                style={styles.imageView}
              />
            </View>
          )}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainContainer: {
    justifyContent: "center",
    flex: 1,
    margin: 5,
    marginTop: Platform.OS === "ios" ? 20 : 0
  },

  imageView: {
    width: "50%",
    height: 100,
    margin: 7,
    borderRadius: 7
  },

  textView: {
    width: "50%",
    textAlignVertical: "center",
    padding: 10,
    color: "#000"
  }
});
